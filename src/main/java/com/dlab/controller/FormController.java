package com.dlab.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.dlab.Application;
import com.dlab.pageManager.PageManager;
import com.dlab.pageManager.pageModel.PageData;

@Controller
public class FormController {
    
    @Autowired
    private PageManager pageManager;;    
    
    /**
     * Map page keys to 'pages' attribute in template
     * @return ArrayList of page keys
     */
    @ModelAttribute("pages")
    public ArrayList<String> pages() {
        ArrayList<String> keys = pageManager.getKeys();
        return keys;
    }
    
    
    /**
     *  Map '/console' GET request to the contentManagement form selector  
     * @param model
     * @param request
     * @param response
     * @return the 'contentManagement' form
     */
    @RequestMapping(value = "/console", method = RequestMethod.GET)
    public String pageForm(HttpServletRequest request, HttpServletResponse response) {
        return "CONTENT_MGMT";
    }
    
    
    /**
     * Map '/console/form' GET request to PAGE_FORM
     * @param pageData  -  Model Attribute
     * @param model
     * @param params  -  Maps all parameters to the 'params' objects
     * @return PAGE_FORM
     */
    @RequestMapping (value = "/console/form", method = RequestMethod.GET)
    public String form(@ModelAttribute PageData pageData, Model model, 
            @RequestParam Map<String,String> params) {
        if(params == null || params.isEmpty()) {
            return "PAGE_FORM";
        }
        if(params.get("page").equals("new")) {
            model.addAttribute("pageData", new PageData());
        }
        else {
            String pageName = params.get("pageName");
            model.addAttribute("pageData", this.pageManager.getPage(pageName));
        }
        return "PAGE_FORM";
    }
    
    
    /**
     * Map '/console/form' POST requests with any 'addPageContent' parameter to this method
     * Method adds a blank pageContent element to the selected page => {pageContent="", pageContentType=""}
     * Page Manager state is then saved to disk (updating JSON also)
     * @param pageData
     * @param model
     * @return PAGE_FORM (same form)
     */
    @RequestMapping(value = "/console/form", params = {"addPageContent"}, method = RequestMethod.POST)
    public String addContentRow(@ModelAttribute PageData pageData, Model model) {
        //System.out.println("*** Testing multi input ***");
        // if the page doens't exist, save it into the manager
        if( pageManager.getPage(pageData.getPageHeading()) == null) {
            pageManager.savePage(pageData);
        }
        pageData.setPageContentEmpty();
        pageManager.getPage(pageData.getPageHeading()).setPageContent(pageData.getPageContent());
        pageManager.saveState();
        return "PAGE_FORM";
    }
    
    /**
     * Map '/console/form' POST requests with any 'removePageContent' parameter to this method
     * Method removes the pageContent element at the requested index 
     * Page Manager state is then saved to disk (updating JSON also)
     * @param pageData
     * @param index  -  pageContent index to remove
     * @return fragments/EXISTING_PAGE_FORM (same form)
     */
    @RequestMapping(value = "/console/form", params = {"removePageContent"}, method = RequestMethod.POST)
    public String removeRow(@ModelAttribute PageData pageData, @RequestParam("removePageContent") int index) {
        //System.out.println("*** Testing multi remove ***");
        // if the page doens't exist, save it into the manager
        if( pageManager.getPage(pageData.getPageHeading()) == null) {
            pageManager.savePage(pageData);
        }
        pageData.removeContent(index);
        pageManager.getPage(pageData.getPageHeading()).setPageContent(pageData.getPageContent());
        pageManager.saveState();
        return "PAGE_FORM";
    }
    
    
    /**
     * Map default '/console/form' POST requests to this method
     * Method saves page into the pageManager (overwriting) 
     * State is saved to disk and corresponding JSON file is updated  
     * @param pageData
     * @param model
     * @param file
     * @return the result of the form submission 
     */
    // need to change @RequestParam to map all parameters
    // currently only uploads background image
    @RequestMapping(value = "/console/form", method = RequestMethod.POST)
    public String pageSubmit(@ModelAttribute PageData pageData, Model model, @RequestParam("background-image") MultipartFile file) {      
        String filename = file.getOriginalFilename(); 
        pageData.setBackgroundImage(filename);
        this.pageManager.savePage(pageData);
        // attempt to save the file if one is included in the upload data
        if (!file.isEmpty()) {
            try {
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(new File(Application.ROOT + "/" + filename)));
                FileCopyUtils.copy(file.getInputStream(), stream);
                stream.close();
            }
            catch (Exception e) {
                System.err.println(e);
            }
        }
        else {
            System.err.println("Failed to upload image or field was empty");
        }
        
        return "RESULT";
        
    }
    
    @RequestMapping(value = "/console/result")
    public String showResult(@ModelAttribute PageData pageData, Model model) {

        return "RESULT";
    }
    
}