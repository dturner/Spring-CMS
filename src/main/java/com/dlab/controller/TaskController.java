package com.dlab.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dlab.utils.JSONOperations;

@Controller
public class TaskController {

    @Autowired
    private JSONOperations jsonOps;
    
    /**
     * Map JSON files to 'tasks' attribute in template
     * @return JSONObject containing file data 
     */
    @ModelAttribute("tasks")
    public JSONObject getJson() {
        return jsonOps.getFiles();
    }
  
    
    /**
     *  Map all '/tasks/{TaskName}' requests to the main template
     *  @return the 'main' AirLearning Template
     */
    @RequestMapping(value = "/tasks/**")
    public String taskRequest(HttpServletRequest request, HttpServletResponse response) {
        return "MAIN";
    }   
}