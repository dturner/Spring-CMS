package com.dlab.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/*
 * Class to load all JSON files into a JSONObject
 * TODO: Add generic types
 */
@Component
public class JSONOperations {
	
    private String BASE;
    private JSONObject files;
    private JSONHelper helper;
	
    @Autowired
    public JSONOperations(@Value("${AirLearning.path}") String path) {
        this.BASE = path;
        this.helper =  new JSONHelper(this.BASE);
        this.files = loadJsonFiles();
    }
	
	
	public JSONObject loadJsonFiles() {
		System.out.println(BASE);
		File baseFolder = new File(BASE);
		File[] folders = baseFolder.listFiles();
		System.out.println(baseFolder.listFiles());
		JSONObject object = new JSONObject();
		for (int i=0; i<folders.length; i++ ){
			File file = folders[i];
			
			if (file.isDirectory()){
				JSONObject obj = loadJsonFile(file.listFiles());
				
				if(obj != null) {
					String taskName = file.getName();
					obj = manipulateJson(obj, taskName);
					object.put(file.getName(), obj);
				}
			}
		}
		return object;	
	}
	
	
	
	/*
	 * load first JSON file in a directory listing
	 * return null if none found
	 */
	private static JSONObject loadJsonFile(File[] files) {
		for (int i=0; i<files.length; i++) {
			File currentFile = files[i];
			
			if(currentFile.isFile() && currentFile.getName().endsWith("json")) {
				try {
					String fileString = readFile(currentFile);
					JSONParser parser = new JSONParser();
					try{
						JSONObject obj = (JSONObject)parser.parse(fileString);	
				 		return obj;
					}
					catch(org.json.simple.parser.ParseException pe){
						System.out.println(pe);
					}
				}
				catch (FileNotFoundException E){
					System.out.println(E);
				}
			}
		}
		return null;
	}
	
	public JSONObject getFiles() {
	    return this.files;
	}
	
	/*
	 * read file lines into a string builder
	 * returns file contents or an empty string
	 */
	public static String readFile(File file) throws FileNotFoundException {
		StringBuilder result = new StringBuilder("");
	    //ClassLoader classloader = getClass().getClassLoader();
		//File file = new File(classloader.getResource(filename).getFile());
	    
	    Scanner scanner = new Scanner(file);
	    	
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			result.append(line).append("\n");
		}

		scanner.close();
			
		String data = result.toString();
		return data;
	}
	
	/*
	 * manipulate JSONOBject for easier use in the Thymeleaf template
	 */
	private JSONObject manipulateJson(JSONObject obj, String taskName) {
		
		// Setup the page heading class type (sets the color)
		String header = (String) obj.get("page-heading");
		String[] bits = header.split(" ");
		String type = bits[bits.length-1];
		obj.put("heading-type", type.toLowerCase());
		
		// setup the page slides if any
		this.helper.setPageSlides(obj, taskName);
		this.helper.setPageContent(obj, taskName);
		this.helper.setPageQuiz(obj, taskName);
		 
		 // setup the background image 
		 if(obj.get("background-image").equals("")) {
		     // no background image
		     obj.put("background", false);
		 }
		 else {
		     // has a background
		     obj.put("background", true);
		 }

		return obj;
	}
	
}
