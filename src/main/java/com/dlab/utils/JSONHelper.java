package com.dlab.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.github.rjeschke.txtmark.Configuration;
import com.github.rjeschke.txtmark.Processor;


/**
 * Class to insert specific properties into a JSONObject
 * the properties are dependent on the contents of the JSON file  
 */
public class JSONHelper {

    public String BASE;
    
    public JSONHelper(String path) {
        this.BASE = path;
        
    }
    // Helper function to setup page slides if any exist in the json object
    public JSONObject setPageSlides(JSONObject obj, String taskName) {
        JSONArray slides = (JSONArray) obj.get("slide-show");
         if (slides.isEmpty()) {
             obj.put("slides", false);
         }
         else {
             obj.put("slides", true);
             // load the markdown texts
             JSONObject t = (JSONObject) slides.get(0);
             JSONArray source = (JSONArray) t.get("source");
             Iterator<JSONObject> iter = source.iterator();
             int length = 0;
             while(iter.hasNext()) {
                 JSONObject currentSlide = (JSONObject) iter.next();
                 
                 JSONArray slideContent = new JSONArray();
                 
                 // load markdown
                 String markdown = (String) currentSlide.get("caption");
                 String [] fileLines = loadMarkdown(BASE + "/" + taskName + "/" + markdown);  
                 
                 for(int i=0; i<fileLines.length; i++){
                     String line = fileLines[i];
                     
                     // add the parsed html to the object
                     String result = Processor.process(line);
                     //System.out.println(result);
                     slideContent.add(result);
                     
                              
                 }
                 currentSlide.put("slideContents", slideContent);
             }
         }
         return obj;
    }
    
    // Helper function to setup page content if any exist in the json object
    public JSONObject setPageContent(JSONObject obj, String taskName) {
        
        JSONArray videos = (JSONArray) obj.get("video");
        if (videos.isEmpty()){
            obj.put("videos", false);
        }
        else {
            obj.put("videos", true);
        }
        
        JSONArray content = (JSONArray) obj.get("page-content"); 
        
        if (content.isEmpty() || content.equals("")) {
             obj.put("content", false);
        }
        else {
            obj.put("content", true);
            Iterator<JSONObject> iter = content.iterator();
            
            JSONArray arr = new JSONArray();
            while(iter.hasNext()) {
                JSONObject currentContent = iter.next();
                String markdownFile = (String) currentContent.get("page-content-path");
                 
                // load markdown file
                String[] fileLines= loadMarkdown(BASE + "/" + taskName + "/" + markdownFile);
                
                JSONArray pageContent = new JSONArray();
                
                // if the loaded markdown is empty
                if (fileLines.length == 1 && (fileLines[0].equals("") || fileLines[0].equals(" "))){
                    obj.put("bowtie", true);
                }
                // otherwise setup the page contents
                else {
                    obj.put("bowtie", false);
                    
                    JSONArray contentData = new JSONArray();
                    
                    for(int i=0; i<fileLines.length; i++){
                        String line = fileLines[i];
                        
                        // add the parsed html to the object
                        String result = Processor.process(line);
                        //System.out.println(result);
                        pageContent.add(result);
                                 
                    }
                    
                }
                currentContent.put("page-content-content", pageContent);
            }
        }
        return obj;
    }
    
 // Helper function to setup page quiz content if any exist in the json object
    public JSONObject setPageQuiz(JSONObject obj, String taskName) {
        
        JSONArray quizz = (JSONArray) obj.get("quizz");
        if (quizz.isEmpty()){
            obj.put("quizz", false);
        }
        else {
            obj.put("quizz", true);
          
            Iterator<JSONObject> iter = quizz.iterator();
            
            JSONArray arr = new JSONArray();
            while (iter.hasNext()) {
                JSONObject currentQuizz = iter.next();
                
                String quizzType = (String) currentQuizz.get("quizz-type");
                if (quizzType.equals("MULTIPLE_CHOICE")) {
                    String markdownFile = (String) currentQuizz.get("quizz-questions");
                    String [] fileLines = loadMarkdown(BASE + "/" + taskName + "/" + markdownFile);
                    // load options into a JSONArray and save into the object
                    for(int i=0; i<fileLines.length; i++){
                        String line = fileLines[i];
                        arr.add(line);
                    }
                    obj.put("quizzOptions", arr);
                    
                    // load answer into the object
                    long correctAnswer = (long) currentQuizz.get("quizz-correct-answer");
                    obj.put("correctAnswer", correctAnswer);
                }
                else if (quizzType.equals("OPEN_QUESTION")) {
                    
                }
            }
        }
        return obj;
    }
    
    
    // function to load markdown into a string array 
    private static String[] loadMarkdown(String filename) {
        try{
            File markdownFile = new File(filename);
            String file = JSONOperations.readFile(markdownFile);
            if (file != ""){
                file = file.replaceAll("(?m)^[ \t]*\r?\n", "");
               String[] fileLines = file.split("\\r?\\n");
               return fileLines;
            }
        }
        catch(FileNotFoundException E){
            System.out.println(E);
        }
        return null;
    }
}
