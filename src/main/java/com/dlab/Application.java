package com.dlab;

import java.io.File;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@SpringBootApplication
public class Application {
    
    public static String ROOT = "upload-dir";
    
    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(5000000);
        factory.setMaxRequestSize(5000000);
        return factory.createMultipartConfig();
    }
    
    public static void main(String[] args) {
        new File(ROOT).mkdir();
        SpringApplication.run(Application.class, args);  
    }
    
    

}