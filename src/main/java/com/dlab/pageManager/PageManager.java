package com.dlab.pageManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.dlab.pageManager.pageModel.PageData;
import com.google.gson.Gson;

@Component
public class PageManager {

    // pages use the task name as the key
    private ConcurrentHashMap<String, PageData> pages;
    
    public PageManager() {
        this.pages = new ConcurrentHashMap<String, PageData>();
        loadState();
    }
    
    
    /**
     * Save a page into the concurrentHashMap
     * Save the state to disk
     * @param page
     */
    public void savePage(PageData page) {
        this.pages.put(page.getPageHeading(), page);
        if (saveState() == false) {
            System.err.println("********************\nFailed to save state\n********************\n");
        }
        else {
            System.out.println("Page hashmap saved");
        }
    }
    
    
    /**
     * @param pageKey
     * @return page with pageKey from the Map
     */
    public PageData getPage(String pageKey) {
        return this.pages.get(pageKey);
    }
    
    
    /**
     * Save the ConcurrentHashMap to disk (pages.data)
     * save the HashMap pages to JSON files
     * @return true if save successful
     * @return false otherwise
     */
    public boolean saveState() {
        Gson gson = new Gson();       
        for (Object value : pages.values()) {
            String json = gson.toJson(value);
            //System.out.println(json);
        }
        
        try {
            FileOutputStream f_out = new FileOutputStream("pages.data");
            ObjectOutputStream obj_out = new ObjectOutputStream (f_out);
            obj_out.writeObject(this.pages);
            obj_out.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    
    /**
     * Update ConcurrentHashMap with previously saved pages.data
     * If pages.data doesn't exist save the current blank ConcurrentHashMap to disk
     */
    public void loadState() {
        try {
            File f = new File("pages.data");
            // save file exists
            if ( f.exists() && !f.isDirectory() ) {
                FileInputStream f_in = new FileInputStream("pages.data");
                ObjectInputStream obj_in = new ObjectInputStream(f_in);
                Object obj = obj_in.readObject();
                obj_in.close();
                if (obj instanceof ConcurrentHashMap<?, ?>) {
                    ConcurrentHashMap <String, PageData> table = (ConcurrentHashMap<String, PageData>) obj;
                    this.pages = table;
                }
                else {
                    System.err.println("Loaded object not a hashmap");
                }
            }
            // save file doesn't exist
            else {
                System.err.println("Save file doesn't esits");
                saveState();
            }
            
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            e.printStackTrace();
        }
    }
    
    
    /**
     * @return ArrayList of page keys
     */
    public ArrayList<String> getKeys() {
        ArrayList<String> keys = new ArrayList<String>();
        for (Entry<String, PageData> entry : this.pages.entrySet()) {
            String key = entry.getKey();
            //PageData data = entry.getValue();
            keys.add(key);
        }
        return keys;
    }
    
    /**
     * @return Pages
     */
    public ConcurrentHashMap<String, PageData> getPages(){
        return this.pages;
    }
}
