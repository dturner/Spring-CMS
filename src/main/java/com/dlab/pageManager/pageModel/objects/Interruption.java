package com.dlab.pageManager.pageModel.objects;

public class Interruption {

	private String interruptionCaption;
	private String interruptionPath;
	
	public Interruption(String interruptionCaption, String interruptionPath) {
		this.interruptionCaption = interruptionCaption;
		this.interruptionPath = interruptionPath;
	}
	
	public String getCaption(){
		return interruptionCaption;
	}
	
	public String getPath() {
		return interruptionPath;
	}
	
}
