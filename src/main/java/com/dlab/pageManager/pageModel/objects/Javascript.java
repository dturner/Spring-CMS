	package com.dlab.pageManager.pageModel.objects;

public class Javascript {

	private String relativePath;
	
	public Javascript(String relativePath) {
		this.relativePath = relativePath;
	}
	
	public String getPath() {
		return relativePath;
	}
}
