package com.dlab.pageManager.pageModel.objects;

public class Source {
	
	private String caption;
	private String image;
	private String download;
	private String link;
	
	public Source(String caption, String image, String download, String link) {
		this.caption = caption;
		this.image = image;
		this.download = download;
		this.link = link;
	}
	
	public String getCaption() {
		return caption;
	}
	
	public String getImage() {
		return image;
	}
	
	public String getDownload() {
		return download;
	}
	
	public String getLink() {
		return link;
	}
}
