package com.dlab.pageManager.pageModel.objects;

public class Quizz {

	private String quizzType;
	private String quizzQuestions;
	private String quizzCorrectAnswer;
	
	public Quizz(String quizzType, String quizzQuestions, String quizzCorrectAnswer) {
		this.quizzType = quizzType;
		this.quizzQuestions = quizzQuestions;
		this.quizzCorrectAnswer = quizzCorrectAnswer;
	}
	
	public String getQuizzType(){
		return quizzType;
	}
	public void setPageContentPath(String newQuizzType){
		quizzType = newQuizzType;
	}
	
	public String getQuizzQuestions() {
		return quizzQuestions;
	}
	public void setQuizzQuestions(String newQuizzQuestions) {
		quizzQuestions = newQuizzQuestions;
	}
	
	public String getModalBackgroundImage() {
		return quizzCorrectAnswer;
	}
	public void setModalBackgroundImage(String newQuizzCorrectAnswer) {
		quizzCorrectAnswer = newQuizzCorrectAnswer;
	}
	
}
