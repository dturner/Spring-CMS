package com.dlab.pageManager.pageModel.objects;

public class Modal {
	private String modalTrigger;
	private String modalContent;
	private String modalBackgroundImage;
	
	public Modal(String modalTrigger, String modalContent, String modalBackgroundImage) {
		this.modalTrigger = modalTrigger;
		this.modalContent = modalContent;
		this.modalBackgroundImage = modalBackgroundImage;
	}
	
	public String getTrigger(){
		return modalTrigger;
	}
	public void setPageContentPath(String newTrigger){
		modalTrigger = newTrigger;
	}
	
	public String getPageContentType() {
		return modalContent;
	}
	public void setPageContentType(String newContent) {
		modalContent = newContent;
	}
	
	public String getModalBackgroundImage() {
		return modalBackgroundImage;
	}
	public void setModalBackgroundImage(String newImage) {
		modalBackgroundImage = newImage;
	}
}
