package com.dlab.pageManager.pageModel.objects;

public class Audio {

	private String audioPath;
	private String 	audioCaption;
	private String audioAutoPlay;
	private String audioLoop;
	private String audioStartAt;
	private String audioEndAt;
	
	public Audio(String audioPath, String audioCaption, String audioAutoPlay,
			String audioLoop, String audioStartAt, String audioEndAt) {
		this.audioPath = audioPath;
		this.audioCaption = audioCaption;
		this.audioAutoPlay = audioAutoPlay;
		this.audioLoop = audioLoop;
		this.audioStartAt = audioStartAt;
		this.audioEndAt = audioEndAt;
	}
	
	public String getPath() {
		return audioPath;
	}
	
	public String getCaption() {
		return audioCaption;
	}

	public String getAutoPlay() {
		return audioAutoPlay;
	}

	public String getLoop() {
		return audioLoop;
	}
	
	public String getStart() {
		return audioStartAt;
	}
	
	public String getEnd() {
		return audioEndAt;
	}
	
}
