package com.dlab.pageManager.pageModel.objects;

import java.util.List;

public class SlideShow {

	private String fullScreen;
	private List<Source> source;
	private String displayDurationInMilliseconds;
	private String transitionDuration;
	
	public SlideShow(String fullScreen, List<Source> source, String displayDurationInMilliseconds, String transitionDuration) {
		this.fullScreen = fullScreen;
		this.source = source;
		this.displayDurationInMilliseconds = displayDurationInMilliseconds;
		this.transitionDuration = transitionDuration;
	}
	
	public String getFullscreen(){
		return fullScreen;
	}
	public void setFullScreen(String fullScreen){
		this.fullScreen = fullScreen;
	}
	
	public List<Source> getSource() {
		return source;
	}
	public void setSource(List<Source> source) {
		this.source = source;
	}
	
	public String getDisplayDuration() {
		return displayDurationInMilliseconds;
	}
	
	public String getTransition() {
		return transitionDuration;
	}
	
}
