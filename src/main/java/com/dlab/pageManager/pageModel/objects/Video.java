package com.dlab.pageManager.pageModel.objects;

public class Video {

	private String videoPath;
	private String 	videoCaption;
	private String videoAutoPlay;
	private String videoFullscreen;
	private String videoLoop;
	private String videoStartAt;
	private String videoEndAt;
	
	public Video(String videoPath, String videoCaption, String videoAutoPlay,
			String videoFullscreen, String videoLoop, String videoStartAt, String videoEndAt) {
		this.videoPath = videoPath;
		this.videoCaption = videoCaption;
		this.videoAutoPlay = videoAutoPlay;
		this.videoFullscreen = videoFullscreen;
		this.videoLoop = videoLoop;
		this.videoStartAt = videoStartAt;
		this.videoEndAt = videoEndAt;
	}
	
	public String getPath() {
		return videoPath;
	}
	
	public String getCaption() {
		return videoCaption;
	}
	public String getAutoPlay() {
		return videoAutoPlay;
	}
	public String getFullscreen() {
		return videoFullscreen;
	}
	public String getLoop() {
		return videoLoop;
	}
	public String getStart() {
		return videoStartAt;
	}
	public String getEnd() {
		return videoEndAt;
	}
	
}
