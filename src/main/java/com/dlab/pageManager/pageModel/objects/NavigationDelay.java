package com.dlab.pageManager.pageModel.objects;

public class NavigationDelay {
	
	private String next;
	private String previous;
	
	public NavigationDelay(String next, String previous) {
		this.next = next;
		this.previous = previous;
	}
	
	public String getQuizzType(){
		return next;
	}
	public void setPageContentPath(String newNext){
		next = newNext;
	}
	
	public String getQuizzQuestions() {
		return previous;
	}
	public void setQuizzQuestions(String newPrevious) {
		previous = newPrevious;
	}

}
