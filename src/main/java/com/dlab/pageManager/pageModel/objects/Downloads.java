package com.dlab.pageManager.pageModel.objects;

public class Downloads {

	private String downloadCaption;
	private String downloadPath;
	
	public Downloads(String downloadCaption, String downloadPath) {
		this.downloadCaption = downloadCaption;
		this.downloadPath = downloadPath;
	}
	
	public String getCaption(){
		return downloadCaption;
	}
	
	public String getPath() {
		return downloadPath;
	}
	
}
