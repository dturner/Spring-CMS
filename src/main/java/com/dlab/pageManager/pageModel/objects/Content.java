package com.dlab.pageManager.pageModel.objects;

import java.io.Serializable;

public class Content implements Serializable{
	
    private static final long serialVersionUID = -4234993640794085422L;
    private String pageContent;
	private String pageContentType;
	public Content() {
	    
	}
	
	public String getPageContent(){
		return this.pageContent;
	}
	public void setPageContent(String content){
		this.pageContent = content;
	}
	
	public String getPageContentType() {
		return this.pageContentType;
	}
	public void setPageContentType(String newType) {
		this.pageContentType = newType;
	}
	
}
