package com.dlab.pageManager.pageModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.dlab.pageManager.pageModel.objects.Content;

public class PageData implements Serializable{
	/**
     * Generated serial version id, used for checking compatibility 
     */
    private static final long serialVersionUID = -2684527253977015297L;
    private String pageTitle;
	private String pageHeading;
	private String pageType;
	private String pageRoleId;
	private String backgroundImage;
	private String template;
    private List<Content> pageContent;
//	private List<Modal> modal;
//	private List<Quizz> quizz;
//	private List<NavigationDelay> navigationDelay;
//	private List<SlideShow> slideShow;
//	private List<Video> video;
//	private List<Audio> audio;
//	private List<Downloads> downloads;
//	private List<Interruption> interuption;
//	private List<Javascript> aditionalJavascript;
	
	public PageData() {
		pageContent = new ArrayList<Content>();
	}
    
	/**
	 * pageTitle setter and getter
	 */
	public void setPageTitle(String title) {
	    this.pageTitle = title;
	}
	public String getPageTitle() {
	    return this.pageTitle;
	}
	
	
	/**
	 * pageHeading setter and getter
	 */
	public void setPageHeading(String heading) {
	    this.pageHeading = heading;	    
	}
	public String getPageHeading() {
	    return this.pageHeading;
	}
	
	
	/**
	 * pageRoleId setter and getter
	 */
	public void setPageRoleId(String roleId) {
	    this.pageRoleId = roleId;
	}
	public String getPageRoleId() {
	    return this.pageRoleId;
	}
	
	
	/**
	 * backgroundImage setter and getter
	 * @param background
	 */
	public void setBackgroundImage(String background) {
	    this.backgroundImage = background;
	}
	public String getBackgroundImage() {
	    return this.backgroundImage;
	}
	
	
	/**
	 * pageType setter and getter
	 * @param type
	 */
	public void setPageType(String type) {
	    this.pageType = type;
	}
	public String getPageType() {
	    return this.pageType;
	}
	
	
	/**
	 * template setter and getter
	 * @param template
	 */
	public void setTemplate(String template) {
	    this.template = template;
	}
	public String getTemplate() {
	    return this.template;
	}
	
	
	/**
	 * pageContent setter and getter
	 * @param content
	 */
	public void setPageContent(List<Content> content) {
	    this.pageContent = content;
	}
	public List<Content> getPageContent() {
        if(this.pageContent == null) {
            this.pageContent = new ArrayList<Content>(); 
        }
        return this.pageContent;
    }
	/**
	 * Create new empty page content
	 */
	public void setPageContentEmpty() {
	    Content content = new Content();
	    content.setPageContent("");
	    content.setPageContentType("normal");
	    this.pageContent.add(content);
	}
	/**
	 * Remove page content at index
	 * @param index
	 */
	public void removeContent(int index) {
	    this.pageContent.remove(index);
	}
}
