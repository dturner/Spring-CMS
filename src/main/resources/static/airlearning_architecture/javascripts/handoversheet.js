function showHandoverSheet()
{
	$().modal('HANDOVERSHEET', 'modals/blank-task-card.html', true, '500px');
	$('#modal').animate({top:'30px', height:'530px'},100);
	$('#closeblackscreen').animate({top:'20px'},100);

	$('#modal').delay(3000).animate({opacity:0}, 1500, function(){
		$('#closeblackscreen').remove();	
		$('#modal').remove();
		$('#blackscreen').animate({opacity:0},500,function(){
			$('#blackscreen').remove();	
		});
		$('.next-arrow').animate({opacity:1},500);
	});
}
$(document).ready(function(){
	showHandoverSheet();						   
});