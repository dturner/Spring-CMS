$(function () {
    setTimeout(playSlideShow, 20000);

    function playSlideShow() {

        var currImgContainer = $('.showImg');
        if (!$(currImgContainer).hasClass('lastImg')) {
            $('.showImg').removeClass('showImg').next().addClass('showImg');
            setTimeout(playSlideShow, 20000);
        }
    }
});