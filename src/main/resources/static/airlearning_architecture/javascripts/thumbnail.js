curSlide = 0;
function playSlideShowX()
{
	var numOfSlides = $('.full-screen-image').length;
	var currentSlide =  $('.full-screen-image:eq('+curSlide+')');
	showSlide(curSlide);
	if(curSlide == numOfSlides - 1)
	{
		curSlide = 0;
	}
	else
	{
		curSlide++;
	}			
}

function moveToSlide(slideIndex)
{
	showSlide(slideIndex);
	curSlide = slideIndex;
}

function showSlide(elIndex)
{
	$('.showImg').removeClass('showImg');
	$('.full-screen-image:eq('+elIndex+')').addClass('showImg');
	$('.current-pager').removeClass('current-pager');
	$('.slidenum:eq('+elIndex+')').addClass('current-pager');
	$('.slide-thumbnails img.active-thumb').removeClass('active-thumb');
	$('.slide-thumbnails img:eq('+elIndex+')').addClass('active-thumb');

}

$(document).ready(function(){
	for(i=0;i<$('.slideShow .full-screen-image').length; i++)
    {
    	var pager="";
    	if(i==0)
    	{
    		pager="current-pager"
    	}
    	else
    	{
    		pager="";
    	}
    	$('.numslides').append('<span class="slidenum '+pager+'">&nbsp;</span>');

    	el = ".full-screen-image:eq("+i+")";
    	imgSrc = $(el).css('background-image');
    	bg = imgSrc.replace('url(','').replace(')','');

    	$('.slide-thumbnails').append('<img src='+ bg +'  height="70" >');
    }	

    $('.slide-thumbnails img:first').addClass('active-thumb');

    $(document).on('click', '.prevSlide', function(){
    	prevSlide = --curSlide;
    	if(prevSlide >= 0)
    	{
    		showSlide(prevSlide);
    	}
    	else
    	{
    		curSlide = 0;
    		showSlide(curSlide);
    	}


    });	
    $(document).on('click', '.nextSlide', function(){
    	nextSlide = ++curSlide;
    	if(nextSlide <  $('.full-screen-image').length)
		{
			showSlide(nextSlide);
		}
		else
		{
			curSlide = $('.full-screen-image').length - 1;
			showSlide(curSlide);
		}

    });	

    $(document).on("click", '.slidenum', function(){
	   moveToSlide($(this).index());
	});
	$(document).on("click", '.slide-thumbnails img', function(){
	   moveToSlide($(this).index());
	});


});