#Landing Gear

**6 July 1996 at 1424 hrs**

A McDonnell Douglas MD-88,N927DA, operated by Delta Air Lines Inc., as flight 1288, experienced an engine failure during the initial part of its takeoff roll on runway 17 at Pensacola Regional Airport in Pensacola,Florida.