#Pylon

**24 April 2010 at 0733 hrs**

After a base maintenance check at Exeter the aircraft was flown uneventfully to East Midlands to be repainted. During the return flight to Exeter the right engine suffered a significant oil leak and lost oil pressure, so the flight crew shut it down.