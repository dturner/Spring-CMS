We should be always questioning items and tasks, in terms of safety. An opportunity to spot the error was lost due to a lack of pro-activity.

While team work and reliance on colleagues is critical, multiple checks and duplicate checks should be inherent.