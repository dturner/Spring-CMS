function zoomBowTie(event)
{
    $(".zoomViewport").zoomViewport();
    $(".zoomContainer").zoomContainer();
    $(".zoomTarget").zoomTo({targetsize:1.2, duration:1000});
}