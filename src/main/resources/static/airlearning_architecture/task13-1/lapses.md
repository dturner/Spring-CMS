A lapse occurs when we forget to complete an action we had been intending to perform. Example, remembering to remove tools at the end of a job.
