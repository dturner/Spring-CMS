var baseurl = 'http://localhost/AirLearningUI/';

$(document).ready(function(){
  positionBoxes( getPercentage($('.box-locate')) * 1);

  $('footer span').click(function(){
    if ($(this).hasClass('first')){
        $('#progress-bar').val('0');
        $(this).nextAll().removeClass('visited');  
        $('.percent').html("0% Complete");
       }else if ($(this).hasClass('second')){
        $(this).nextAll().removeClass('visited');  
        $('#progress-bar').val('14');
        $(this).prevAll().addClass('visited');  
        $(this).addClass('visited');
         $('.percent').html("33% Complete");
       }else if ($(this).hasClass('third')){
        $(this).nextAll().removeClass('visited');  
        $('#progress-bar').val('28');
        $(this).prevAll().addClass('visited'); 
        $(this).addClass('visited');
        $('.percent').html("66% Complete");
       }else if ($(this).hasClass('fourth')){
        $(this).nextAll().removeClass('visited');  
        $('#progress-bar').val('42');
        $(this).prevAll().addClass('visited'); 
        $(this).addClass('visited');
        $('.percent').html("66% Complete");
       }else if ($(this).hasClass('fifth')){
        $(this).nextAll().removeClass('visited');  
        $('#progress-bar').val('57');
        $(this).prevAll().addClass('visited'); 
        $(this).addClass('visited');
        $('.percent').html("66% Complete");
       }else if ($(this).hasClass('sixth')){
        $(this).nextAll().removeClass('visited');  
        $('#progress-bar').val('71');
        $(this).prevAll().addClass('visited'); 
        $(this).addClass('visited');
        $('.percent').html("66% Complete");
       }else if ($(this).hasClass('seventh')){
        $(this).nextAll().removeClass('visited');  
        $('#progress-bar').val('85');
        $(this).prevAll().addClass('visited'); 
        $(this).addClass('visited');
        $('.percent').html("66% Complete");
       } else{
        $('#progress-bar').val('100');
         $(this).addClass('visited');
        $(this).prevAll().addClass('visited');
         $('.percent').html("100% Complete");
       }
  }); //end click

 $(window).resize(function(){
  
   $percentage = getPercentage($('.box-locate')) * 1; 
   positionBoxes($percentage);
   

  }); //end resize


  if($('.draggable').length >=1)
  {
    $(".draggable").draggable({
      revert: "invalid",
      drag: function( event, ui ) {
        $('.failure-sequence-submit-li').addClass('drag-here');
      },
      stop: function( event, ui ) {
        $('.failure-sequence-submit-li').removeClass('drag-here');
      }
    });
  }
  if($(".droplist").length >= 1)
  {
    $(".droplist").droppable({
        accept: ".draggable",
        activeClass: "droparea-highlight",
        drop: function( event, ui ) {
          // console.log("dropped");
          // console.log(ui.draggable);
          ui.draggable.animate({width:($('.droparea').width() * .75)},500);
          $('.failure-sequence-submit-li').before('<li>'+ui.draggable.context.innerHTML+'</li>');
          //$('.droplist li div').css(['position','relative'],['top',''],['left','']);
          ui.draggable.remove();

          checkListItems($('.droplist li'), 8, $('#failure-sequence-submit'));
        }
      }).sortable();//end droppable droparea
  }
    

  $(document).on('click', '#failure-sequence-submit', function(){

      var failure_sequence = '';
      prefix = '';
      $('.droplist li p').each(function(){
        failure_sequence += prefix + '' + $(this).text();
        prefix = ',';
      });
      

      var url = "NARESH SERVICE THAT HANDLES THE SEQUENCE DETAILS";
      
      //below swap the '' with url after you updated the above value
      $.ajax({
        url: '',
        type: 'POST',
        data:{ failureSequence: failure_sequence},
        success:function(data){

          $('#failure-sequence-submit').animate({opacity:0}, 500, function(){

            
            $('.droplist li').each(function(){
              $(this).stop().delay(500).animate({opacity:0.6}, 500);
            });
          });

          $('.droplist').sortable("disable");
        }
      });

  });

});// end document ready


function getPercentage(img)
{
  var optimalWidth = 1340;
  percentage = null;
  if(img.length >= 1)
  {
    //console.log(img.length);
    percentage = img.css('width').replace(/[^-\d\.]/g, '') / optimalWidth;  
  }
  return percentage;
}


function positionBoxes(percentage){
  //console.log(percentage);

  
  $('.box1').css('right', Math.ceil(220 * percentage) + "px");
  $('.box1').css('top', Math.ceil(120 * percentage) + "px");

  $('.box2').css('right', Math.ceil(770 * percentage) + "px");
  $('.box2').css('top', Math.ceil(305 * percentage) + "px");

  $('.box3').css('right', Math.ceil(180 * percentage) + "px"); 
  $('.box3').css('top', Math.ceil(300 * percentage) + "px");

  $('.box4').css('right', Math.ceil(510 * percentage) + "px"); 
  $('.box4').css('top', Math.ceil(425 * percentage) + "px");

  $('.box5').css('right', Math.ceil(875 * percentage) + "px"); 
  $('.box5').css('top', Math.ceil(380 * percentage) + "px");

  $('.box6').css('right', Math.ceil(680 * percentage) + "px"); 
  $('.box6').css('top', Math.ceil(370 * percentage) + "px");

  $('.box7').css('right', Math.ceil(380 * percentage) + "px"); 
  $('.box7').css('top', Math.ceil(385 * percentage) + "px");

  $('.box8').css('right', Math.ceil(845 * percentage) + "px"); 
  $('.box8').css('top', Math.ceil(455 * percentage) + "px");
}


function checkListItems(el, num, hiddenelement)
{
  
  if(el.length == num)
  {
    hiddenelement.stop().animate({opacity:1}, 500);
  }
}

function hideSlowly(el)
{
  el.animate({opacity:0.6}, 500);
}


$(document).ready(function(){
        $(document).on('click','.text-button',function(){

            if($('.text-image').length > 0)
            {

                var button = $(this);

                var showOrhide = $('.text-image').css('opacity');
                var imgSrc = 'img/text-button.jpg';

                if(showOrhide == 1)
                {
                  showOrhide = 0;
                }
                else
                {
                  showOrhide = 1;
                }

                button.animate({opacity:.01}, 50, function(){

                    $('.text-image').stop().animate({opacity:showOrhide}, 250, function(){
                        if(showOrhide == 0)
                        {
                          imgSrc = 'img/text-button-reverse.jpg';
                        }

                        button.css('background-image',"url("+ baseurl + "/" + imgSrc+")");

                        button.animate({opacity:1}, 50, function(){});
                    });
                                    
                });


            }
                   
        });

       
    });



