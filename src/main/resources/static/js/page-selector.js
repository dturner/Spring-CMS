
$("#template").change(function() {
	//update the corresponding values for template here
	        
	var template = $("#template").val();
	
	switch (template) {
	
	case "COMMON_TEMPLATE":
		$("#general_area").css("display", "");
		$("#navigation_area").css("display", "");
		$("#content_area").css("display", "");
		$("#model_area").css("display", "none");
		$("#quiz_area").css("display", "none");
		$("#slide_area").css("display", "none");
		$("#video_area").css("display", "none");
		$("#audio_area").css("#display", "none");
		$("#downloads_area").css("display", "none");
		$("#javascript_area").css("display", "");
		break;
		
	case "DOWNLOADS_TEMPLATE":
		$("#general_area").css("display", "");
		$("#navigation_area").css("display", "");
		$("#content_area").css("display", "");
		$("#model_area").css("display", "none");
		$("#quiz_area").css("display", "none");
		$("#slide_area").css("display", "none");
		$("#video_area").css("display", "none");
		$("#audio_area").css("#display", "none");
		$("#downloads_area").css("display", "");
		$("#javascript_area").css("display", "");
		break;
	
	case "QUIZZ_TEMPLATE":
		$("#general_area").css("display", "");
		$("#navigation_area").css("display", "");
		$("#content_area").css("display", "");
		$("#model_area").css("display", "none");
		$("#quiz_area").css("display", "");
		$("#slide_area").css("display", "none");
		$("#video_area").css("display", "none");
		$("#audio_area").css("#display", "none");
		$("#downloads_area").css("display", "none");
		$("#javascript_area").css("display", "");
		break;
		
	case "TOP_RIGHT_CORNER":
		$("#general_area").css("display", "");
		$("#navigation_area").css("display", "");
		$("#content_area").css("display", "");
		$("#model_area").css("display", "none");
		$("#quiz_area").css("display", "none");
		$("#slide_area").css("display", "");
		$("#video_area").css("display", "none");
		$("#audio_area").css("#display", "none");
		$("#downloads_area").css("display", "none");
		$("#javascript_area").css("display", "");
		break;
		
	case "TOP_RIGHT_CORNER_INFO":
		$("#general_area").css("display", "");
		$("#navigation_area").css("display", "");
		$("#content_area").css("display", "");
		$("#model_area").css("display", "none");
		$("#quiz_area").css("display", "none");
		$("#slide_area").css("display", "none");
		$("#video_area").css("display", "none");
		$("#audio_area").css("#display", "none");
		$("#downloads_area").css("display", "none");
		$("#javascript_area").css("display", "none");
		break;
	
	default: 
		
	}
	
});