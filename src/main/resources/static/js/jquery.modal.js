//Modal//

/**
* | Modal plugin by 
* | Joris Vreeke
* | More information on http://www.jorisvreeke.com/modal/
* | © 2011 All Rights Reserved
*/

(function ($) {
 
	
	jQuery.fn.modal =  function(modaltitle, content, loadPage, maxWidth){
	
		var blackscreen = '<div id="blackscreen"></div>';
		var modal = '<div id="closeblackscreen" title="close">x</div><div id="modal"><div id="modalcontent"></div></div>';
		$('body').prepend(blackscreen);
		$('body').prepend(modal);
		$('body').append('<div id="offsetter"></div>');
		
		
		/**
		* adds the modal to the page.
		* @modaltitle = [Text] the title you want to display give '' if you want to leave the title blank
		* @content = [HTML] What is displayed in the body of your modal or provide the url of the page that holds the content you want to display in your modal 
		* @loadPage = [bool] true = load the content from the page you specified in @content, false = display the text you have specified in @content
		* @width = [int] the width in pixels 
		*/
		function addModal(modaltitle, content, loadPage, maxWidth)
		{
			
			// if you want to load the content from another page or file
			if(loadPage)
			{
				
				//shows preload image until actual content is loaded
				$('#modalcontent').html('<div style="width:16px; height:16px; margin:20px auto;"><img src="/img/loading.gif" alt="loading" /></div>');
				
				//remove the loading image and show the content
				$('#modalcontent').load(content, function(){
					if(modaltitle.length>0)
					{
						$('#modal').prepend('<div id="modaltitle">'+modaltitle+'</div>');
					}	
					//position in the right place
					styleDivs(maxWidth);
				});
			}
			// if you just want to show some HTML
			else
			{
				$('#modalcontent').html(content);
				if(modaltitle.length>0)
				{
					$('#modal').prepend('<div id="modaltitle">'+modaltitle+'</div>');
				}	
				styleDivs(maxWidth);
			}
		}
		
		
		
		/**
		*	positions the modal in the centre of the page
		*  @maxWidth = [int] Maximum width of the modal in pixels
		*/
		function styleDivs(maxWidth)
		{
			maxW = parseInt(maxWidth) * 1;
			
			//set the default width to 300 if we cant find a valid value in @maxWidth
			if(maxW <= 0 || isNaN(maxW))
			{
				maxW = 300;
			}
			
			var wHeight = $(window).height();
			var wWidth = $(window).width();		
			var mHeight = $('#modalcontent').height() + $('#modaltitle').height() + 20;
			var mWidth = maxW;
			var top = ((wHeight - mHeight) / 2) + $('#offsetter').offset().top;
			
			if (top < 0)
			{
				top = 20;
			}
			
			
			//the semi transparent background
			$('#blackscreen').css({
				"height" : $(window).height() + "px",
				"width" : $(window).width() + "px",
				"opacity" : 0.4
			});
			
			
			//the actual message box
			$('#modal').css({
				"width" : mWidth + "px",
				"height" : mHeight,
				"left": (wWidth - mWidth) / 2 + 'px',
				"top":top + 'px'
			});
			
			//the button that closes the modal
			$('#closeblackscreen').css({
				"left": (((wWidth - mWidth) / 2) + $('#modal').width()) + 30 +'px',
				"top": top -10 +'px',
				"opacity" : 1
			});
		
			
			//make sure the width of an image does not exceed the width of the modal
			if($('#modal img').width() >= mWidth)
			{
				$('#modal img').css({
					'width' : (mWidth) + 'px'
				});
			}
		}
		
		//add the modal to the page
		addModal(modaltitle, content, loadPage, maxWidth);
		
		//position it at the centre of the page
		styleDivs(maxWidth);
		
		//also when the user resizes their browser window
		$(window).bind("resize", function(){  
			styleDivs(maxWidth);
		});
		
		//make the modal disappear on clicking outside the modal or when the x is clicked
		$('#blackscreen, #closeblackscreen').bind('click',function(){
			$('#closeblackscreen').remove();	
			$('#modal').remove();
			$('#blackscreen').animate({opacity:0},500,function(){
				$('#blackscreen').remove();	
			});		
		});
		
		//indicate that the x in the top right hand corner is clickable 
		$('#closeblackscreen').hover(
			function(){
				$(this).addClass('closeblackscreen_over');
			},
			function(){
				$(this).removeClass('closeblackscreen_over');
			}
		);
		
	};
})(jQuery);